package es.unican.alejandro.preciogasolineras.Presentacion;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collection;
import java.util.Collections;

import es.unican.alejandro.preciogasolineras.Negocio.DatosGasolineras;
import es.unican.alejandro.preciogasolineras.R;
import es.unican.alejandro.preciogasolineras.Datos.RemoteFetch;

public class PrecioGasolinaActivity extends AppCompatActivity {
    TextView textView;
    DatosGasolineras datosGasolineras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_precio_gasolina);
        textView = (TextView) findViewById(R.id.textGasolinera);
        textView.setMovementMethod(new ScrollingMovementMethod());
        //Obteniendo los datos de las gasolineras
        textView.setText(getResources().getString(R.string.obteniendo));
        datosGasolineras = new DatosGasolineras();
        boolean res = datosGasolineras.obtenGasolineras();
        if(res){
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.datos_exito), Toast.LENGTH_LONG).show();
            textView.setText(datosGasolineras.getTextoGasolineras());
        }else{
            textView.setText(getResources().getString(R.string.datos_no_obtenidos));
        }//if
    }//onCreate
}// PrecioGasolinaActivity
