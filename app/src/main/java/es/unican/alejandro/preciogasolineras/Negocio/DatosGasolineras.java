package es.unican.alejandro.preciogasolineras.Negocio;

import android.util.Log;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.List;

import es.unican.alejandro.preciogasolineras.Datos.Gasolinera;
import es.unican.alejandro.preciogasolineras.Datos.ParserJSON;
import es.unican.alejandro.preciogasolineras.Datos.RemoteFetch;

/**
 * Created by alejandro on 29/09/16.
 */

public class DatosGasolineras {

    private List<Gasolinera> listaGasolineras;
    private RemoteFetch remoteFetch;

    public DatosGasolineras(){
        remoteFetch = new RemoteFetch();
    }//Constructor

    /**
     * Método a través del cual se almacenan las gasolineras en el atributo listaGasolineras
     * de esta clase. Para realizar esto internamente realiza una llamada a la función
     * getJSON (RemoteFetch) para seguidamente parsear el JSON obtenido con la llamada
     * a readJSonStream (ParserJSON)
     * @return
     */
    public boolean obtenGasolineras(){
        try {
            remoteFetch.getJSON();
            listaGasolineras = ParserJSON.readJsonStream(remoteFetch.getBufferedDataGasolineras());
            Log.d("ENTRA", "Obten gasolineras:"+listaGasolineras.size());
            return true;
        }catch(Exception e){
            Log.e("ERROR","Error en la obtención de gasolineras: "+e.getMessage());
            e.printStackTrace();
            return false;
        }//try
    }//obtenGasolineras

    public List<Gasolinera> getListaGasolineras() {
        return listaGasolineras;
    }//getListadoGasolineras


    public void setListaGasolineras(List<Gasolinera> listaGasolineras) {
        this.listaGasolineras = listaGasolineras;
    }//setListadoGasolineras

    /**
     * Método para obtener un cadena de texto con todas las gasolineras. En esta cadena
     * se muestra el rótulo, la dirección, la localidad, el precio del diésel y el precio
     * de la gasolina 95
     * @return String con todas las gasolineras separadas por un doble salto de línea
     */
    public String getTextoGasolineras(){
        String textoGasolineras="";
        if(listaGasolineras!=null){
            for (int i=0; i<listaGasolineras.size(); i++){
                textoGasolineras=textoGasolineras+listaGasolineras.get(i).getRotulo()+"\n"+
                        listaGasolineras.get(i).getDireccion()+"\n"+
                        listaGasolineras.get(i).getLocalidad()+"\n"+
                        "Precio diesel: "+listaGasolineras.get(i).getGasoleo_a()+" "+"\n"+
                        "Precio gasolina 95: "+listaGasolineras.get(i).getGasolina_95()+" "+"\n\n";
            }//for
        }else{
            textoGasolineras="Sin gasolineras";
        }//if
        return textoGasolineras;
    }//getSantanderGasolineras

    /**
     * Ordena las gasolineras por precio
     * Si queremos modificar el tipo de ordenación deberiamos modificar el método CompareTo
     * en la clase Gasolinera
     */
    public void ordenaGasolinerasPorPrecio(){
        Collections.sort(listaGasolineras);
    }//ordenaGasolineras

    /**
     * Metodo que retorna las estadísticas del conjunto de gasolineras
     * @return Cadena de texto con información relevante relacionada con las estadísticas
     */
    public String getEstadisticas(){
        double precioMedioDiesel=0.0;
        double precioMedioGasolina=0.0;
        String estadisticas;
        for (int i = 0; i<listaGasolineras.size(); i++){
            precioMedioDiesel=precioMedioDiesel+listaGasolineras.get(i).getGasoleo_a();
            precioMedioGasolina=precioMedioGasolina+listaGasolineras.get(i).getGasolina_95();
        }
        precioMedioDiesel=precioMedioDiesel/listaGasolineras.size();
        precioMedioGasolina=precioMedioGasolina/listaGasolineras.size();
        Gasolinera gBarata=listaGasolineras.get(0);
        Gasolinera gCara=listaGasolineras.get(listaGasolineras.size()-1);
        //Para el redondeo a dos cifras de los double
        DecimalFormat df = new DecimalFormat("0.00");
        df.setRoundingMode(RoundingMode.DOWN);
        estadisticas="El precio medio de la gasolina es: "+df.format(precioMedioDiesel)+"€/L\n"+
                "El precio medio del diésel es: "+df.format(precioMedioGasolina)+"€/L\n\n"+
                "La gasolinera más barata es de la marca: "+gBarata.getRotulo()+"\n"+
                "Diesel: "+df.format(gBarata.getGasoleo_a())+"€/L\n"+
                "Gasolina: "+df.format(gBarata.getGasolina_95())+"€/L\n\n"+
                "La diferencia con la gasolinera más cara en diesel es de "+(df.format((gCara.getGasoleo_a()-gBarata.getGasoleo_a())))+"€/L"+
                " y en gasolina de "+(df.format((gCara.getGasolina_95()-gBarata.getGasolina_95())))+"€/L";
        return estadisticas;

    }//getEstadisticas


}//DatosGasolineras
